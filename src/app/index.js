(function() {
    'use strict';

    angular.module('singApp', [
		'ui.router',
		'firebase',

        'singApp.core',
        'singApp.dashboard',
        'singApp.form.elements',
        'singApp.maps.google',
        'singApp.maps.vector',
        'singApp.grid',
        'singApp.tables.basic',
        'singApp.tables.dynamic',
        'singApp.extra.calendar',
        'singApp.extra.invoice',
        'singApp.login',
        'singApp.error',
        'singApp.extra.gallery',
        'singApp.extra.search',
        'singApp.extra.timeline',
        'singApp.ui.components',
        'singApp.ui.notifications',
        'singApp.ui.icons',
        'singApp.ui.buttons',
        'singApp.ui.tabs-accordion',
        'singApp.ui.list-groups',
        'singApp.inbox',
		'singApp.groups',
		'singApp.channels',
		'singApp.members',
		'singApp.overview',
		// 'singApp.groupsoverview',
		/*'singApp.channelsoverview',
		'singApp.groupsoverview',
		'singApp.useroverview',*/
		'singApp.map',
		'singApp.profile',
        'singApp.widgets',
        'singApp.charts',
		'singApp.invite',
        'singApp.form.validation',
        'singApp.form.wizard'
    ])
	.config(configFunction)
    .run(runFunction);

  configFunction.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configFunction($stateProvider, $urlRouterProvider) {
	$stateProvider.state("otherwise", {
		url: "/login",
		templateUrl: "app/modules/login/login.html"
	});
  }

  runFunction.$inject = ['$rootScope', '$location'];

  function runFunction($rootScope, $location) {
    $rootScope.$on('$routeChangeError', function(event, next, previous, error) {
      if (error === "AUTH_REQUIRED") {
        $location.path('/');
      }
    });
  }


})();
