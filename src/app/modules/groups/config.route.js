(function() {
  'use strict';

  angular
    .module('singApp.groups', ['ui.router', 'ui.bootstrap'])
    .config(configFunction);

  configFunction.$inject = ['$stateProvider'];

  function configFunction($stateProvider) {    
    $stateProvider
      .state('app.groups', {
        url: '/groups',
        templateUrl: 'app/modules/groups/groups.html',
        controller: 'GroupsListController',
		resolve: {user: resolveUser}
      })
	  .state('app.detail', {
			url: '/group/:groupKey', 
			templateUrl: 'app/modules/groups/groupsoverview.html',
			controller: 'GroupOController',
			resolve: {user: resolveUser}
		})

  }

  resolveUser.$inject = ['authService'];

  function resolveUser(authService) {
    return authService.firebaseAuthObject.$requireAuth();
  }

})();
