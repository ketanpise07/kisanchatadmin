(function() {
  'use strict';
  
angular.module('singApp.groups').filter('startFrom', function () {
	return function (input, start) {
		if (input) {
			start = +start;
			return input.slice(start);
		}
		return [];
	};
});
  
  angular.module('singApp.groups')
	.controller('GroupsListController', GroupsListController)
	.controller('GroupOController', GroupOController);
  
  GroupsListController.$inject = ['$scope', '$rootScope', 'GroupService', 'filterFilter'];
  function GroupsListController ($scope, $rootScope, GroupService, filterFilter) {
		$scope.pageNum = 1;
		$scope.pageLimit = 5;
		$scope.recStarts = $scope.pageNum*$scope.pageLimit;
		$scope.GroupData = '';
		var grpData = [];
		
		var init = function () {
			
			// $scope.pageItems = GroupService.getCommunities('number');
			// $scope.pageItems.$loaded().then(function (response) {
				// $scope.Group = $scope.pageItems;
			// });
			
			var grpCnt = GroupService.getGroupsCnt();
			grpCnt.$loaded().then(function (response1) {
				$scope.pagesTot = grpCnt.length;
				$scope.lastPage = Math.ceil(grpCnt.length/$scope.pageLimit);
			});
			

			var syncGrpNames = GroupService.getGroupsNames();
			var syncGrpCnt = GroupService.getGroupsMemCount();
			
			syncGrpNames.$loaded().then(function (response1) {
				$scope.GroupList = response1;
				syncGrpCnt.$loaded().then(function (response2) {
					angular.forEach(response1, function(group1) {
					   angular.forEach(response2, function(group2) {
							var grpDetails = {groupName:'', totalMembers:''};
							if(group1.$id===group2.$id) {
								grpDetails.info = group1;
								if(group1.communityImageSmallThumbUrl !== '') {
									grpDetails.profileImage = group1.communityImageSmallThumbUrl;
								} else {
									grpDetails.profileImage = "assets/images/people/a2.jpg";
								}
								
								if(group2.users) {
									grpDetails.totalMembers = Object.keys(group2.users).length;
								}
								grpData.push(grpDetails);
							}
					   });
					});
					$scope.items = grpData;//startCall();
					$scope.search = {};

					$scope.resetFilters = function () {
						// needs to be a function or it won't trigger a $watch
						$scope.search = {};
					};
			
					// pagination controls
					$scope.currentPage = 1;
					$scope.totalItems = Object.keys($scope.items).length;
					$scope.entryLimit = 20; // items per page
					$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
					
					// $watch search to update pagination
					$scope.$watch('search', function (newVal, oldVal) {
						$scope.filtered = filterFilter($scope.items, newVal);
						$scope.totalItems = Object.keys($scope.filtered).length;
						$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
						$scope.currentPage = 1;
					}, true);
				});
				
			});
		};
		
		init();
		
		// create empty search model (object) to trigger $watch on update
		$scope.setItemsPerPage = function(num) {
		  $scope.entryLimit = num;
		  $scope.currentPage = 1; //reset to first paghe
		}
		
		
		
	
		$scope.blockToggle = function(index, info) {
				if(info.communityStatus === "" || info.communityStatus === "active") {
					info.communityStatus = "blocked";
				} else {
					info.communityStatus = "active";
				}
				$scope.GroupList.$save(info);
		}
		
		$scope.markGroupFeatured = function(index, info) {
				info.featured = 1-info.featured;
				$scope.GroupList.$save(info);
			};

  }

  
  GroupOController.$inject = ['$scope', '$rootScope', '$stateParams', 'GroupService'];
  function GroupOController ($scope, $rootScope, $stateParams, GroupService) {
		
		var initGrp = function () {
			var grpData = GroupService.getGroupDetails($stateParams.groupKey);
			var adminData = GroupService.getGroupAdminDetails($stateParams.groupKey);
			grpData.$loaded().then(function (response) {
				$scope.communityDesc = response.communityDesc;
				$scope.groupKey = $stateParams.groupKey;
				$scope.featured = response.featured;
				$scope.communityStatus = response.communityStatus;
								
				if(response.communityImageBigThumbUrl) {
					$scope.communityImageBigThumbUrl = response.communityImageBigThumbUrl;
				} else {
					$scope.communityImageBigThumbUrl = "assets/images/pictures/19.jpg";
				}
				if(response.communityImageSmallThumbUrl) {
					$scope.communityImageSmallThumbUrl = response.communityImageSmallThumbUrl;
				} else {
					$scope.communityImageBigThumbUrl = "assets/images/people/a5.jpg";
				}
				
				if(response.communityImageUrl) {
					$scope.communityImageUrl = response.communityImageUrl;
				} else {
					$scope.communityImageBigThumbUrl = "assets/images/people/a5.jpg";
				}
				
				$scope.communityName = response.communityName;
				$scope.communityOwnerId = response.communityOwnerId;
				$scope.communityOwnerName = response.communityOwnerName;
				$scope.privacy = response.privacy;
				$scope.type = response.type;
				
				adminData.$loaded().then(function (admin) {
					if(admin.users) {
						$scope.presentMembers = Object.keys(admin.users).length;
					}
					angular.forEach(admin.users, function(grpadmin) {
							
							if($scope.communityOwnerId == grpadmin.mobile) {
								$scope.adminName = grpadmin.firstName+' '+grpadmin.lastName;
								if(grpadmin.imageUrl) {
									$scope.adminImgUrl = grpadmin.imageUrl;
								} else {
									$scope.adminImgUrl = "assets/images/people/a5.jpg";
								}
							}
							
					});
				});
				
			});
		};
		
		initGrp();
		
		
		$scope.markGroupFeatured = function() {
				var community = GroupService.getCommunityByKey($scope.groupKey);
				community.$loaded().then(function (response) {
					community.featured = 1-community.featured;
					$scope.featured = community.featured;
					community.$save();
				});
		};
		
		
		$scope.blockToggle = function(groupKey) {
				var groupId = groupKey;
				if(groupId===""){
					groupId = $scope.groupKey;
				}
				var community = GroupService.getCommunityByKey(groupId);
				community.$loaded().then(function (response) {
					if(community.communityStatus === "" || community.communityStatus === "active") {
						community.communityStatus = "blocked";
					} else {
						community.communityStatus = "active";
					}
					$scope.communityStatus = community.communityStatus;
					community.$save();
				});
		};
		
	}
  
})();
