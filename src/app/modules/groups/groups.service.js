(function() {
  'use strict';

  angular
    .module('singApp.groups')
    .factory('GroupService', GroupService);

  GroupService.$inject = ['$firebaseAuth', 'firebaseDataService', '$firebaseArray', '$firebaseObject', 'FIREBASE_URL', '$q'];

  function GroupService($firebaseAuth, firebaseDataService, $firebaseArray, $firebaseObject, FIREBASE_URL, $q) {
    var firebaseAuthObject = $firebaseAuth(firebaseDataService.root);

    var service = {
      firebaseAuthObject: firebaseAuthObject,
	  getGroupsNames: getGroupsNames,
	  getGroupsMemCount: getGroupsMemCount,
	  getGroupsCnt: getGroupsCnt,
	  getGroupDetails: getGroupDetails,
	  getGroupAdminDetails: getGroupAdminDetails,
	  getCommunityByKey: getCommunityByKey,
	  getCommunities: getCommunities
    };

    return service;
	
	function getGroupsCnt() {
		var ref = new Firebase(FIREBASE_URL);
		var grpObj1 = $firebaseArray(ref.child('communities').orderByChild('type').equalTo('g'));
		return grpObj1;
	}
	
	function getGroupsNames() {
		var ref = new Firebase(FIREBASE_URL);
		var grpObj1 = $firebaseArray(ref.child('communities').orderByChild('type').equalTo('g'));
		return grpObj1;
	}
	
	function getGroupsMemCount() {
		var ref = new Firebase(FIREBASE_URL);
		var grpObj2 = $firebaseArray(ref.child('communityObjects').child('communityUsers'));
		return grpObj2;
	}
	
	function getGroupDetails(groupKey) {
		var ref = new Firebase(FIREBASE_URL);
		var grpdata = $firebaseObject(ref.child('communities').child(groupKey));
		return grpdata;
	}
	
	function getGroupAdminDetails(groupKey) {
		var ref = new Firebase(FIREBASE_URL);
		var admindata = $firebaseObject(ref.child('communityObjects').child('communityUsers').child(groupKey));
		return admindata;
	}

	function getCommunityByKey(groupKey) {
		var ref = new Firebase(FIREBASE_URL);
		var obj = $firebaseObject(ref.child('communities').child(groupKey));
		return obj;
	}
	
	function getCommunities(field) {
		//var ref = new Firebase(FIREBASE_URL+"/communities");
		var fb = new Firebase(FIREBASE_URL);
		var norm = new Firebase.util.NormalizedCollection(
			fb.child('communities')
		);
		norm = norm.select( {key: 'communities.type.$value', alias: 'commtype'}, 'communities.communityName', 'communities.profileImage', 'communities.communityStatus', 'communities.featured' );
		norm = norm.filter(
			function(data, key, priority) { return data.commtype === 'g'; }
		);
		var ref = norm.ref();

		// create a Paginate reference
		var pageRef = new Firebase.util.Paginate(ref, field, {maxCacheSize: 250});
		// generate a synchronized array using the special page ref
		var list = $firebaseArray(pageRef);
		// store the "page" scope on the synchronized array for easy access
		list.page = pageRef.page;

		// when the page count loads, update local scope vars
		pageRef.page.onPageCount(function(currentPageCount, couldHaveMore) {
		  list.pageCount = currentPageCount;
		  list.couldHaveMore = couldHaveMore;
		});

		// when the current page is changed, update local scope vars
		pageRef.page.onPageChange(function(currentPageNumber) {
		  list.currentPageNumber = currentPageNumber;
		});

		// load the first page
		pageRef.page.next();

		return list;
		//return 0;
	}
	
  }

})();
