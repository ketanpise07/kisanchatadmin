(function() {
  'use strict';
//https://kisanchattesting.firebaseio.com
  angular
    .module('singApp.core')
    .constant('FIREBASE_URL', 'https://kisanchatforketan.firebaseio.com/')
    .constant('PROTECTED_PATHS', ['/app/dashboard', '/app/overview', '/app/groups', '/app/channels', '/app/members']);

})();
