(function() {
  'use strict';

  angular
    .module('singApp.login')
    .controller('AuthController', AuthController);

  AuthController.$inject = ['$scope', '$location', 'authService'];

  function AuthController($scope, $location, authService) {
    $scope.message = null;
    $scope.error = null;

	$scope.register = function() {
		return authService.register($scope.email, $scope.password)
		.then(function(userData) {
			$scope.message = "User created with uid: " + userData.uid;
		}).catch(function(error) {
			$scope.error = error;console.log(error);
		});
    };

    $scope.login = function() { $scope.auth_error ="";
		return authService.login($scope.email, $scope.password)
		.then(function(authData) {
			$location.path('/app/overview');
		}).catch(function(error) {
			$scope.error = error;
			$scope.auth_error = "Username or Password is wrong.";
		});
    };

	$scope.logout = function() {console.clear();
		authService.logout();
		$location.path('/login');
    };

  }

})();