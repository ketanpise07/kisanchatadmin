(function() {
  'use strict';

  angular
    .module('singApp.login')
    .factory('authService', authService);

  authService.$inject = ['$firebaseAuth', 'firebaseDataService'];

  function authService($firebaseAuth, firebaseDataService) {
    var firebaseAuthObject = $firebaseAuth(firebaseDataService.root);

    var service = {
      firebaseAuthObject: firebaseAuthObject,
      register: register,
      login: login,
      logout: logout,
      isLoggedIn: isLoggedIn
    };

    return service;

    ////////////

    function register(emailid, passwd) {
      return firebaseAuthObject.$createUser({email: emailid, password: passwd});
    }

    function login(emailid, passwd) {
      return firebaseAuthObject.$authWithPassword({email: emailid, password: passwd});
    }

    function logout() {
      firebaseAuthObject.$unauth();
    }

    function isLoggedIn() {
      return firebaseAuthObject.$getAuth();
    }


  }

})();