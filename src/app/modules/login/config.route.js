(function() {
  'use strict';

  angular
    .module('singApp.login', ['ui.router','firebase'])
    .config(configFunction)
    .run(runFunction);

  configFunction.$inject = ['$stateProvider'];

  function configFunction($stateProvider) {    
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/modules/login/login.html'
      })
  }

  runFunction.$inject = ['$location', 'authService', 'firebaseDataService', 'PROTECTED_PATHS'];

  function runFunction($location, authService, firebaseDataService, PROTECTED_PATHS) {

    firebaseDataService.root.onAuth(function(authData) {
      if (!authData && pathIsProtected($location.path())) {
        authService.logout();
        $location.path('/login');
      };
    });

    function pathIsProtected(path) {
      return PROTECTED_PATHS.indexOf(path) !== -1;
    }
  }

})();
