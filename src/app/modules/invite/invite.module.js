(function() {
  'use strict';

  var module = angular.module('singApp.invite', [
    'ui.router'
  ]);

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
    $stateProvider
      .state('app.invite', {
        url: '/invite',
        templateUrl: 'app/modules/invite/invite.html'
      })
  }
})();
