(function() {
  'use strict';

  var module = angular.module('singApp.map', [
    'ui.router',
    'ui.bootstrap'
  ]);

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
    $stateProvider
      .state('app.map', {
        url: '/map',
        templateUrl: 'app/modules/map/map.html'
      })
  }
})();
