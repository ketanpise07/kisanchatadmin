(function() {
  'use strict';

  angular
    .module('singApp.overview', ['ui.router', 'ui.bootstrap'])
    .config(configFunction);

  configFunction.$inject = ['$stateProvider'];

  function configFunction($stateProvider) {    
    $stateProvider
      .state('app.overview', {
        url: '/overview',
        templateUrl: 'app/modules/overview/overview.html',
        controller: 'OverviewController',
		resolve: {user: resolveUser}
      })
  }

  resolveUser.$inject = ['authService'];

  function resolveUser(authService) {
    return authService.firebaseAuthObject.$requireAuth();
  }

})();
