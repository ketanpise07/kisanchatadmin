(function() {
  'use strict';

  angular.module('singApp.overview')
    .controller('OverviewController', OverviewController)
	.controller('ChannelController', ChannelController)
	.controller('GroupController', GroupController);

  OverviewController.$inject = ['$scope', 'OverviewService'];
  function OverviewController ($scope, OverviewService) {
		var syncObject = OverviewService.getMembers();
		syncObject.$loaded(function() {
				var count = syncObject.length;
				$scope.present_members = count;
				var a_count = 0, i_count = 0, b_count = 0, l_count = 0;
				
				angular.forEach(syncObject, function(syncMem) {
					if(syncMem.userStatus === "active") {
						a_count++;
					} else if(syncMem.userStatus === "inactive") {
						i_count++;
					} else if(syncMem.userStatus === "blocked") {
						b_count++;
					} else if(syncMem.userStatus === "left") {
						l_count++;
					}
				});
				$scope.active_members = a_count;
				$scope.inactive_members = i_count;
				$scope.blocked_members = b_count;
				$scope.left_members = l_count;
		});
		
		$scope.memFilter = function() {
			$scope.groupFilter = function() {
			var selctedSpan = $scope.filtergroups;
			var curDate = new Date();
			var toTs = curDate.getTime();
			var fromTs = toTs;
			if(selctedSpan==1) {
				fromTs = curDate.getTime() - 30*24*60*60*1000;
			} else if(selctedSpan==2) {
				fromTs = curDate.getTime() - 7*24*60*60*1000;
			} else if(selctedSpan==3) {
				var curDate2 = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate(), 0, 0, 0, 0);
				var curDate3 = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate(), 23, 59, 59, 999);
				
				fromTs = curDate2.getTime();
				toTs = curDate3.getTime();
			} else{
				fromTs = toTs = null;
			}
			var count=0, allcount = 0;
			var a_count = 0, i_count = 0, b_count = 0, l_count = 0;
			angular.forEach(syncGrpObject, function(syncGrp) {
					if(fromTs<=syncGrp.registrationTimestamp && syncGrp.registrationTimestamp<=toTs) {
							count++;
							if(syncGrp.userStatus === "active") {
								a_count++;
							} else if(syncGrp.userStatus === "inactive") {
								i_count++;
							} else if(syncGrp.userStatus === "blocked") {
								b_count++;
							} else if(syncGrp.userStatus === "left") {
								l_count++;
							}
					}	
					allcount++;
					
					
					
			});
			if(selctedSpan==0) {
				$scope.present_members = allcount;
			} else {
				$scope.present_members = count;
			}
			$scope.active_members = a_count;
			$scope.inactive_members = i_count;
			$scope.blocked_members = b_count;
			$scope.left_members = l_count;
			
		};

		};
		// count.$bindTo($scope, "present_members");
		// console.log(count);
  }
  
  GroupController.$inject = ['$scope', 'OverviewService'];
  function GroupController ($scope, OverviewService) {
		var syncGrpObject = OverviewService.getGroups();
		syncGrpObject.$loaded(function() {
				var count = syncGrpObject.length;
				var a_count = 0, i_count = 0, b_count = 0, l_count = 0;
				$scope.grp_present = count;
				
				angular.forEach(syncGrpObject, function(syncGrp) {
					if(syncGrp.communityStatus === "active") {
						a_count++;
					} else if(syncGrp.communityStatus === "inactive") {
						i_count++;
					} else if(syncGrp.communityStatus === "blocked") {
						b_count++;
					} else if(syncGrp.communityStatus === "left") {
						l_count++;
					}
				});
				$scope.grp_active = a_count;
				$scope.grp_inactive = i_count;
				$scope.grp_blocked = b_count;
				$scope.grp_left = l_count;
		});
		
		$scope.groupFilter = function() {
			var selctedSpan = $scope.filtergroups;
			var curDate = new Date();
			var toTs = curDate.getTime();
			var fromTs = toTs;
			if(selctedSpan==1) {
				fromTs = curDate.getTime() - 30*24*60*60*1000;
			} else if(selctedSpan==2) {
				fromTs = curDate.getTime() - 7*24*60*60*1000;
			} else if(selctedSpan==3) {
				var curDate2 = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate(), 0, 0, 0, 0);
				var curDate3 = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate(), 23, 59, 59, 999);
				
				fromTs = curDate2.getTime();
				toTs = curDate3.getTime();
			} else{
				fromTs = toTs = null;
			}
			var count=0, allcount = 0;
			var a_count = 0, i_count = 0, b_count = 0, l_count = 0;
			angular.forEach(syncGrpObject, function(syncGrp) {
					if(fromTs<=syncGrp.communityCreatedTimestamp && syncGrp.communityCreatedTimestamp<=toTs) {
							count++;
							if(syncGrp.communityStatus === "active") {
								a_count++;
							} else if(syncGrp.communityStatus === "inactive") {
								i_count++;
							} else if(syncGrp.communityStatus === "blocked") {
								b_count++;
							} else if(syncGrp.communityStatus === "left") {
								l_count++;
							}
					}	
					allcount++;
					
					
					
			});
			if(selctedSpan==0) {
				$scope.grp_present = allcount;
			} else {
				$scope.grp_present = count;
			}
			$scope.grp_active = a_count;
			$scope.grp_inactive = i_count;
			$scope.grp_blocked = b_count;
			$scope.grp_left = l_count;
			
		};
		
  }
  
    
  ChannelController.$inject = ['$scope', 'OverviewService'];
  function ChannelController ($scope, OverviewService) {
		var syncChnObject = OverviewService.getChannels();
		syncChnObject.$loaded(function() {
				var count = syncChnObject.length;
				var a_count = 0, i_count = 0, b_count = 0, l_count = 0;
				$scope.chn_present = count;
				
				angular.forEach(syncChnObject, function(syncGrp) {
					if(syncGrp.communityStatus === "active") {
						a_count++;
					} else if(syncGrp.communityStatus === "inactive") {
						i_count++;
					} else if(syncGrp.communityStatus === "blocked") {
						b_count++;
					} else if(syncGrp.communityStatus === "left") {
						l_count++;
					}
				});
				$scope.chn_active = a_count;
				$scope.chn_inactive = i_count;
				$scope.chn_blocked = b_count;
				$scope.chn_left = l_count;
		});
		
		$scope.channelFilter = function() {
			var selctedSpan = $scope.filterchannels;
			var curDate = new Date();
			var toTs = curDate.getTime();
			var fromTs = toTs;
			if(selctedSpan==1) {
				fromTs = curDate.getTime() - 30*24*60*60*1000;
			} else if(selctedSpan==2) {
				fromTs = curDate.getTime() - 7*24*60*60*1000;
			} else if(selctedSpan==3) {
				var curDate2 = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate(), 0, 0, 0, 0);
				var curDate3 = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate(), 23, 59, 59, 999);
				
				fromTs = curDate2.getTime();
				toTs = curDate3.getTime();
			} else{
				fromTs = toTs = null;
			}
			var count=0, allcount = 0;
			var a_count = 0, i_count = 0, b_count = 0, l_count = 0;
			angular.forEach(syncChnObject, function(syncGrp) {
					if(fromTs<=syncGrp.communityCreatedTimestamp && syncGrp.communityCreatedTimestamp<=toTs) {
							count++;
							if(syncGrp.communityStatus === "active") {
								a_count++;
							} else if(syncGrp.communityStatus === "inactive") {
								i_count++;
							} else if(syncGrp.communityStatus === "blocked") {
								b_count++;
							} else if(syncGrp.communityStatus === "left") {
								l_count++;
							}
					}	
					allcount++;
					
					
					
			});
			if(selctedSpan==0) {
				$scope.chn_present = allcount;
			} else {
				$scope.chn_present = count;
			}
			$scope.chn_active = a_count;
			$scope.chn_inactive = i_count;
			$scope.chn_blocked = b_count;
			$scope.chn_left = l_count;
			
		};
		
  }

})();
