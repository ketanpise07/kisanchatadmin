(function() {
  'use strict';

  angular
    .module('singApp.overview')
    .factory('OverviewService', OverviewService);

  OverviewService.$inject = ['$firebaseAuth', 'firebaseDataService', '$firebaseObject', '$firebaseArray', 'FIREBASE_URL'];

  function OverviewService($firebaseAuth, firebaseDataService, $firebaseObject, $firebaseArray, FIREBASE_URL) {
    var firebaseAuthObject = $firebaseAuth(firebaseDataService.root);

    var service = {
      firebaseAuthObject: firebaseAuthObject,
      getMembers: getMembers,
	  getGroups: getGroups,
	  getChannels: getChannels
    };

    return service;
    
    function getMembers() {
		var ref = new Firebase(FIREBASE_URL);
		return $firebaseArray(firebaseDataService.users);
		
    }
	
	function getGroups() {
		var ref = new Firebase(FIREBASE_URL);
		return $firebaseArray(ref.child('communities').orderByChild('type').equalTo('g'));
    }

	function getChannels() {
		var ref = new Firebase(FIREBASE_URL);
		return $firebaseArray(ref.child('communities').orderByChild('type').equalTo('c'));
    }

  }

})();
