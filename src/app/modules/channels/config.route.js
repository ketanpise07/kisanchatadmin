(function() {
  'use strict';

  angular
    .module('singApp.channels', ['ui.router', 'ui.bootstrap'])
    .config(configFunction);

  configFunction.$inject = ['$stateProvider'];

  function configFunction($stateProvider) {    
    $stateProvider
      .state('app.channels', {
        url: '/channels',
        templateUrl: 'app/modules/channels/channels.html',
        controller: 'ChannelsListController',
		resolve: {user: resolveUser}
      })
	  .state('app.channeldetail', {
			url: '/channel/:groupKey', 
			templateUrl: 'app/modules/channels/channelsoverview.html',
			controller: 'ChannelOController',
			resolve: {user: resolveUser}
		})

  }

  resolveUser.$inject = ['authService'];

  function resolveUser(authService) {
    return authService.firebaseAuthObject.$requireAuth();
  }

})();
