(function() {
  'use strict';

angular.module('singApp.channels').filter('startFrom', function () {
	return function (input, start) {
		if (input) {
			start = +start;
			return input.slice(start);
		}
		return [];
	};
});  
  
  angular.module('singApp.channels')
	.controller('ChannelsListController', ChannelsListController)
	.controller('ChannelOController', ChannelOController);
  
  ChannelsListController.$inject = ['$scope', 'ChannelService', 'filterFilter'];
  function ChannelsListController ($scope, ChannelService, filterFilter) {
		$scope.pageNum = 1;
		$scope.pageLimit = 5;
		$scope.recStarts = $scope.pageNum*$scope.pageLimit;
		var chnData = [];
		
		var init = function () {
			var grpCnt = ChannelService.getChannelsCnt();
			grpCnt.$loaded().then(function (response1) {
				$scope.pagesTot = grpCnt.length;
				$scope.lastPage = Math.ceil(grpCnt.length/$scope.pageLimit);
			});
			

			var syncGrpNames = ChannelService.getChannelsNames();
			var syncGrpCnt = ChannelService.getChannelsMemCount();
			
			syncGrpNames.$loaded().then(function (response1) {
				$scope.GroupList = response1;
				syncGrpCnt.$loaded().then(function (response2) {
					angular.forEach(response1, function(group1) {
					   angular.forEach(response2, function(group2) {
							var grpDetails = {groupName:'', totalMembers:''};
							if(group1.$id===group2.$id) {
								grpDetails.info = group1;
								if(group1.communityImageSmallThumbUrl !== '') {
									grpDetails.profileImage = group1.communityImageSmallThumbUrl;
								} else {
									grpDetails.profileImage = "assets/images/people/a2.jpg";
								}
								
								if(group2.users) {
									grpDetails.totalMembers = Object.keys(group2.users).length;
								}
								chnData.push(grpDetails);
							}
					   });
					});
					$scope.items = chnData;//startCall();
					$scope.search = {};

					$scope.resetFilters = function () {
						// needs to be a function or it won't trigger a $watch
						$scope.search = {};
					};
			
					// pagination controls
					$scope.currentPage = 1;
					$scope.totalItems = Object.keys($scope.items).length;
					$scope.entryLimit = 20; // items per page
					$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
					
					// $watch search to update pagination
					$scope.$watch('search', function (newVal, oldVal) {
						$scope.filtered = filterFilter($scope.items, newVal);
						$scope.totalItems = Object.keys($scope.filtered).length;
						$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
						$scope.currentPage = 1;
					}, true);
				});
				
			});
		};
		
		init();
		
				
		// create empty search model (object) to trigger $watch on update
		$scope.setItemsPerPage = function(num) {
		  $scope.entryLimit = num;
		  $scope.currentPage = 1; //reset to first paghe
		}
	
		$scope.blockToggle = function(index, info) {
				if(info.communityStatus === "" || info.communityStatus === "active") {
					info.communityStatus = "blocked";
				} else {
					info.communityStatus = "active";
				}
				$scope.GroupList.$save(info);
		}
		
		$scope.markGroupFeatured = function(index, info) {
				info.featured = 1-info.featured;
				$scope.GroupList.$save(info);
			};

  }

  
  ChannelOController.$inject = ['$scope', '$stateParams', 'ChannelService'];
  function ChannelOController ($scope, $stateParams, ChannelService) {
		
		var initGrp = function () {
			var chnData = ChannelService.getChannelDetails($stateParams.groupKey);
			var adminData = ChannelService.getChannelAdminDetails($stateParams.groupKey);
			chnData.$loaded().then(function (response) {
				$scope.communityDesc = response.communityDesc;
				
				if(response.communityImageBigThumbUrl) {
					$scope.communityImageBigThumbUrl = response.communityImageBigThumbUrl;
				} else {
					$scope.communityImageBigThumbUrl = "assets/images/pictures/19.jpg";
				}
				if(response.communityImageSmallThumbUrl) {
					$scope.communityImageSmallThumbUrl = response.communityImageSmallThumbUrl;
				} else {
					$scope.communityImageBigThumbUrl = "assets/images/people/a5.jpg";
				}
				
				if(response.communityImageUrl) {
					$scope.communityImageUrl = response.communityImageUrl;
				} else {
					$scope.communityImageBigThumbUrl = "assets/images/people/a5.jpg";
				}
				
				$scope.communityName = response.communityName;
				$scope.communityOwnerId = response.communityOwnerId;
				$scope.communityOwnerName = response.communityOwnerName;
				$scope.privacy = response.privacy;
				$scope.type = response.type;
				
				adminData.$loaded().then(function (admin) {
					if(admin.users) {
						$scope.presentMembers = Object.keys(admin.users).length;
					}
					angular.forEach(admin.users, function(grpadmin) {
							
							if($scope.communityOwnerId == grpadmin.mobile) {console.log($scope.communityOwnerId+"=="+grpadmin.mobile);
								$scope.adminName = grpadmin.firstName+' '+grpadmin.lastName;
								if(grpadmin.imageUrl) {
									$scope.adminImgUrl = grpadmin.imageUrl;
								} else {
									$scope.adminImgUrl = "assets/images/people/a5.jpg";
								}
							}
							
					});
				});
				
			});
		};
		
		initGrp();
		
		$scope.markGroupFeatured = function() {
				var community = GroupService.getCommunityByKey($scope.groupKey);
				community.$loaded().then(function (response) {
					community.featured = 1-community.featured;
					$scope.featured = community.featured;
					community.$save();
				});
		};
		
		
		$scope.blockToggle = function(groupKey) {
				var groupId = groupKey;
				if(groupId===""){
					groupId = $scope.groupKey;
				}
				var community = GroupService.getCommunityByKey(groupId);
				community.$loaded().then(function (response) {
					if(community.communityStatus === "" || community.communityStatus === "active") {
						community.communityStatus = "blocked";
					} else {
						community.communityStatus = "active";
					}
					$scope.communityStatus = community.communityStatus;
					community.$save();
				});
		};
	}
  
})();
