(function() {
  'use strict';

  angular
    .module('singApp.channels')
    .factory('ChannelService', ChannelService);

  ChannelService.$inject = ['$firebaseAuth', 'firebaseDataService', '$firebaseArray', '$firebaseObject', 'FIREBASE_URL', '$q'];

  function ChannelService($firebaseAuth, firebaseDataService, $firebaseArray, $firebaseObject, FIREBASE_URL, $q) {
    var firebaseAuthObject = $firebaseAuth(firebaseDataService.root);

    var service = {
      firebaseAuthObject: firebaseAuthObject,
	  getChannelsNames: getChannelsNames,
	  getChannelsMemCount: getChannelsMemCount,
	  getChannelsCnt: getChannelsCnt,
	  getChannelDetails: getChannelDetails,
	  getChannelAdminDetails: getChannelAdminDetails,
	  getChannelByKey: getChannelByKey
    };

    return service;
	
	function getChannelsCnt() {
		var ref = new Firebase(FIREBASE_URL);
		var grpObj1 = $firebaseArray(ref.child('communities').orderByChild('type').equalTo('c'));
		return grpObj1;
	}
	
	function getChannelsNames() {
		var ref = new Firebase(FIREBASE_URL);
		var grpObj1 = $firebaseArray(ref.child('communities').orderByChild('type').equalTo('c'));
		return grpObj1;
	}
	
	function getChannelsMemCount() {
		var ref = new Firebase(FIREBASE_URL);
		var grpObj2 = $firebaseArray(ref.child('communityObjects').child('communityUsers'));
		return grpObj2;
	}
	
	function getChannelDetails(groupKey) {
		var ref = new Firebase(FIREBASE_URL);
		var grpdata = $firebaseObject(ref.child('communities').child(groupKey));
		return grpdata;
	}
	
	function getChannelAdminDetails(groupKey) {
		var ref = new Firebase(FIREBASE_URL);
		var admindata = $firebaseObject(ref.child('communityObjects').child('communityUsers').child(groupKey));
		return admindata;
	}
	
	function getChannelByKey(groupKey) {
		var ref = new Firebase(FIREBASE_URL);
		var obj = $firebaseObject(ref.child('communities').child(groupKey));
		return obj;
	}

  }

})();
