(function() {
  'use strict';

  angular
    .module('singApp.members', ['ui.router', 'ui.bootstrap'])
    .config(configFunction);

  configFunction.$inject = ['$stateProvider'];

  function configFunction($stateProvider) {    
    $stateProvider
      .state('app.members', {
        url: '/members',
        templateUrl: 'app/modules/members/members.html',
        controller: 'MembersListController',
		resolve: {user: resolveUser}
      })
	  .state('app.memberdetail', {
			url: '/member/:memberid', 
			templateUrl: 'app/modules/members/membersoverview.html',
			controller: 'MemberController',
			resolve: {user: resolveUser}
		})

  }

  resolveUser.$inject = ['authService'];

  function resolveUser(authService) {
    return authService.firebaseAuthObject.$requireAuth();
  }

})();
