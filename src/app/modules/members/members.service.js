(function() {
  'use strict';

  angular
    .module('singApp.members')
    .factory('MemberService', MemberService);

  MemberService.$inject = ['$firebaseAuth', 'firebaseDataService', '$firebaseArray', '$firebaseObject', 'FIREBASE_URL', '$q'];

  function MemberService($firebaseAuth, firebaseDataService, $firebaseArray, $firebaseObject, FIREBASE_URL, $q) {
    var firebaseAuthObject = $firebaseAuth(firebaseDataService.root);

    var service = {
      firebaseAuthObject: firebaseAuthObject,
	  getMembers: getMembers,
	  getMemberDetails: getMemberDetails
    };

    return service;
	
	function getMembers() {
		var ref = new Firebase(FIREBASE_URL);
		var grpObj1 = $firebaseArray(ref.child('users'));
		return grpObj1;
	}
	
	
	function getMemberDetails(memberid) {
		var ref = new Firebase(FIREBASE_URL);
		var memberdata = $firebaseObject(ref.child('users').child(memberid));
		return memberdata;
	}

  }

})();
