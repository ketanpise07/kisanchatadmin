(function() {
  'use strict';

angular.module('singApp.members').filter('startFrom', function () {
	return function (input, start) {
		if (input) {
			start = +start;
			return input.slice(start);
		}
		return [];
	};
});
  
  angular.module('singApp.members')
	.controller('MembersListController', MembersListController)
	.controller('MemberController', MemberController);
  
  MembersListController.$inject = ['$scope', 'MemberService', 'filterFilter'];
  function MembersListController ($scope, MemberService, filterFilter) {
		$scope.pageNum = 1;
		$scope.pageLimit = 5;
		$scope.recStarts = $scope.pageNum*$scope.pageLimit;
		$scope.GroupData = '';
		var memData = [];
		
		var init = function () {
			var members = MemberService.getMembers();
			members.$loaded().then(function (response) {
				$scope.MemberList = response;
				$scope.pagesTot = members.length;
				$scope.lastPage = Math.ceil(members.length/$scope.pageLimit);
				//memData = response;
				angular.forEach(response, function(membs) {
						//membs.info = membs;console.log(membs);
							if(!membs.imageUrl) {
								membs.imageUrl = "assets/images/people/a2.jpg";
							}
							memData.push(membs);
					   });
					$scope.items = memData;//startCall();
					$scope.search = {};

					$scope.resetFilters = function () {
						// needs to be a function or it won't trigger a $watch
						$scope.search = {};
					};
			
					// pagination controls
					$scope.currentPage = 1;
					$scope.totalItems = Object.keys($scope.items).length;
					$scope.entryLimit = 20; // items per page
					$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
					
					// $watch search to update pagination
					$scope.$watch('search', function (newVal, oldVal) {
						$scope.filtered = filterFilter($scope.items, newVal);
						$scope.totalItems = Object.keys($scope.filtered).length;
						$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
						$scope.currentPage = 1;
					}, true);
			});
			
		};
		
		init();
		
		// create empty search model (object) to trigger $watch on update
		$scope.setItemsPerPage = function(num) {
		  $scope.entryLimit = num;
		  $scope.currentPage = 1; //reset to first paghe
		}
	
	
		$scope.blockToggle = function(index, info) {
				if(info.userStatus === "" || info.userStatus === "active") {
					info.userStatus = "blocked";
				} else {
					info.userStatus = "active";
				}
				$scope.MemberList.$save(info);
		}
		
		
  }

  
  MemberController.$inject = ['$scope', '$stateParams', 'MemberService'];
  function MemberController ($scope, $stateParams, MemberService) {
		
		var initGrp = function () {
			var adminData = MemberService.getMemberDetails($stateParams.memberid);
			adminData.$loaded().then(function (response) {
				$scope.memberid = response.$id; 
				$scope.email = response.email; 
				$scope.name = response.firstName + " " + response.lastName;
				
				if(response.imageUrl) {
					$scope.imageUrl = response.imageUrl;
				} else {
					$scope.imageUrl = "assets/images/people/a5.jpg";
				}
				
				$scope.mobile = response.mobile;
				$scope.username = response.username;
				$scope.sso_id = response.sso_id;
				
			});
		};
		
		initGrp();
		
		$scope.blockToggle = function() {
			var member = MemberService.getMemberDetails($scope.memberid);
			member.$loaded().then(function (response) {
				if(member.userStatus === "" || member.userStatus === "active") {
					member.userStatus = "blocked";
				} else {
					member.userStatus = "active";
				}
				$scope.userStatus = member.userStatus;
				member.$save();
			});
		};
		
	}
  
})();
