(function() {
  'use strict';

  angular
    .module('singApp.groupsoverview')
    .factory('GroupService', GroupService);

  GroupService.$inject = ['$firebaseAuth', 'firebaseDataService', '$firebaseArray', 'FIREBASE_URL', '$q'];

  function GroupService($firebaseAuth, firebaseDataService, $firebaseArray, FIREBASE_URL, $q) {
    var firebaseAuthObject = $firebaseAuth(firebaseDataService.root);

    var service = {
      firebaseAuthObject: firebaseAuthObject,
	  getGroupsDetails: getGroupsDetails
    };

    return service;
	
	function getGroupsDetails(groupkey) {
		var ref = new Firebase(FIREBASE_URL);
		var grpObj1 = $firebaseArray(ref.child('communities').child(groupkey));
		return grpObj1;
	}



  }

})();
