(function() {
  'use strict';

  angular
    .module('singApp.groupsoverview', ['ui.router', 'ui.bootstrap'])
    .config(configFunction);

  configFunction.$inject = ['$stateProvider'];

  function configFunction($stateProvider) {    
    $stateProvider
      .state('app.groupsoverview', {
        url: '/group/:groupKey',
        templateUrl: 'app/modules/groupsoverview/groupsoverview.html',
        controller: 'GroupController',
		resolve: {user: resolveUser}
      })
  }

  resolveUser.$inject = ['authService'];

  function resolveUser(authService) {
    return authService.firebaseAuthObject.$requireAuth();
  }

})();
